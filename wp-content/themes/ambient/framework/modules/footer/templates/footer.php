</div> <!-- close div.content_inner -->
	</div>  <!-- close div.content -->
		<footer <?php ambient_elated_class_attribute($footer_classes); ?>>
			<div class="eltdf-footer-inner clearfix">
				<?php
					if($display_footer_top) {
						ambient_elated_get_footer_top();
					}
					if($display_footer_bottom) {
						ambient_elated_get_footer_bottom();
					}
				?>
			</div>
		</footer>
	</div> <!-- close div.eltdf-wrapper-inner  -->
</div> <!-- close div.eltdf-wrapper -->
<?php wp_footer(); ?>

<script type="text/javascript">
	jQuery( document ).ready(function() {
		jQuery('.textarea-cover.field-cover.has-input textarea').on('keypress', (function() {
	        var length = jQuery('.textarea-cover.field-cover.has-input textarea').val().length;
	        console.log(length);
	        if(length >= 0){
	            jQuery('.textarea-cover.field-cover.has-input').addClass('hide-before');
	        }else{
	            jQuery('.textarea-cover.field-cover.has-input').removeClass('hide-before');
	        }
	    }));
	});
</script>
</body>
</html>