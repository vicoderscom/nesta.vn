<?php

/*** Child Theme Function  ***/

function ambient_elated_child_theme_enqueue_scripts() {
	$parent_style = 'ambient_elated_default_style';

	wp_enqueue_style($parent_style, get_template_directory_uri() . '/style.css');

	wp_enqueue_style('ambient_elated_child_style',
		get_stylesheet_directory_uri() . '/style.css',
		array($parent_style),
		wp_get_theme()->get('Version')
	);
}

add_action( 'wp_enqueue_scripts', 'ambient_elated_child_theme_enqueue_scripts' );


/**
* tạo footer info
*/
class footerinfo extends WP_Widget {
    function __construct() {
        parent::__construct(
            'Footer-info',
            'Footer Info',
            array( 'description'  =>  'Footer Info' )
        );
    }
    function form( $instance ) {
        $default = array(
            'title' => '',
            'showroom' => '',
            'email' => '',
            'nha_may' => '',
            'number' => '',
        );
        $instance = wp_parse_args( (array) $instance, $default );
        $title = esc_attr($instance['title']);
        $showroom = esc_attr($instance['showroom']);
        $email = esc_attr($instance['email']);
        $nha_may = esc_attr($instance['nha_may']);
        $number = esc_attr($instance['number']);
        echo '<p>Tiêu đề:<input type="text" class="widefat" name="'.$this->get_field_name('title').'" value="'.$title.'"/></p>';
        echo '<p>Showroom:<input type="text" class="widefat" name="'.$this->get_field_name('showroom').'" value="'.$showroom.'" /></p>';
        echo '<p>Email:<input type="text" class="widefat" name="'.$this->get_field_name('email').'" value="'.$email.'" /></p>';
        echo '<p>Nhà Máy:<input type="text" class="widefat" name="'.$this->get_field_name('nha_may').'" value="'.$nha_may.'" /></p>';
        echo '<p>Số điện thoại:<input type="text" class="widefat" name="'.$this->get_field_name('number').'" value="'.$number.'" /></p>';
    }
    function update( $new_instance, $old_instance ) {
        $instance = $old_instance;
        $instance['title'] = strip_tags($new_instance['title']);
        $instance['showroom'] = ($new_instance['showroom']);
        $instance['email'] = ($new_instance['email']);
        $instance['nha_may'] = ($new_instance['nha_may']);
        $instance['number'] = ($new_instance['number']);
        return $instance;
    }
    function widget( $args, $instance ) {
        extract($args);
        $title = apply_filters( 'widget_title', $instance['title'] );
        $showroom = $instance['showroom'];
        $email = $instance['email'];
        $nha_may = $instance['nha_may'];
        $number = $instance['number'];

        echo $before_widget;
            echo "<div class='textwidget' style='color:#ececec;'>";
				echo "<ul class='list-contact'>";
				echo "<li style='margin: 0;'><i class='fa fa-map-marker'></i><strong>&nbsp; Showroom :</strong> ".$showroom."</li>";
				echo "<li style='margin: 0;'><i class='fa fa-map-marker'></i><strong>&nbsp; Nhà máy :</strong> ".$nha_may."</li>";
				echo "<li style='margin: 0;'><i class='fa fa-envelope-o'></i><strong>&nbsp; Email :</strong> ".$email."</li>";
				echo "<li style='margin: 0;'><i class='fa fa-phone'></i><strong>&nbsp; Điện Thoại :</strong> ".$number." </li>";
			echo "</ul></p></div>";
        echo $after_widget;
    }
}
function create_contact_header_widget() {
    register_widget('footerinfo');
}
add_action( 'widgets_init', 'create_contact_header_widget' );

new footerinfo();